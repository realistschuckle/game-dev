#include "EventMapper.hpp"

Command::~Command() {}

EventMapper::EventMapper() {}

EventMapper::~EventMapper() {
  for (auto hit = _handlers.begin(); hit != _handlers.end(); ++hit) {
    delete (*hit).second;
  }
}

void EventMapper::add(sf::Keyboard::Key input, Command* handler) {
  _handlers[input] = handler;
}

void EventMapper::commit() {
  auto command = _handlers[_currentKey];
  if (command) {
    command->invoke();
  }
}

void EventMapper::handle(sf::Keyboard::Key input) {
  if (_handlers[input]) {
    _currentKey = input;
  }
}
