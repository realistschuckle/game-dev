#if !defined(LEVEL01_HPP)
#define LEVEL01_HPP

#include "../Level.hpp"
#include "Level02.hpp"

class Level01 : public Level {
 public:
  Level01(int side, int width, int height) : Level(side, width, height,
    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    "X                                      X"
    "X                                      X"
    "X                                      X"
    "X                                      X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X           O                          X"
    "X                                      X"
    "X                                      X"
    "X                                      X"
    "X       O                              X"
    "X                                      X"
    "X                                      X"
    "X                                      X"
    "X                                      X"
    "X                                      X"
    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
  ), _next(side, width, height) {}

  virtual Level* getNext() {
    return &_next;
  }

 private:
  Level02 _next;
};

#endif // LEVEL01_HPP
