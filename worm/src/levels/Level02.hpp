#if !defined(LEVEL02_HPP)
#define LEVEL02_HPP

#include "../Level.hpp"

class Level02 : public Level {
 public:
  Level02(int side, int width, int height) : Level(side, width, height,
    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    "X                                      X"
    "X                                      X"
    "X                                      X"
    "X                 XX                   X"
    "X            O    XX                   X"
    "X                 XX           OO      X"
    "X                 XX         OO        X"
    "X                 XX                   X"
    "X                 XX                   X"
    "X                 XX O  XXXXXXXXX      X"
    "X                 XX    XXXXXXXXX      X"
    "X                 XX    XXXXXXXXX      X"
    "X        O        XX    XXXXXXXXX      X"
    "X       XXXXXXXXXXXXXXXXXXXXXXXXX      X"
    "X       XXXXXXXXXXXXXXXXXXXXXXXXX      X"
    "X                 XX     O             X"
    "X                 XX                   X"
    "X                 XX                   X"
    "X                 XX          O        X"
    "X                 XX           O       X"
    "X                 XX            O      X"
    "X                 XX                   X"
    "X       O         XX                   X"
    "X                 XX                   X"
    "X                 XX O                 X"
    "X                                      X"
    "X                                      X"
    "X                                      X"
    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
  ) {}

  virtual Level* getNext() {
    return nullptr;
  }
};

#endif // LEVEL02_HPP
