#include <cstring>
#include "Level.hpp"

Level::Level(int side, int width, int height, const char* description)
    : _numberOfEdibles(0), _width(width), _side(side) {
  _description = new char[strlen(description)];
  strncpy(_description, description, strlen(description));
  if (static_cast<int>(strlen(description)) != width * height) {
    std::cerr << strlen(description) << " != " << width << "*" << height
              << std::endl;
    throw "error";
  }
  for (int i = 0; i < width * height; i += 1) {
    if (description[i] != ' ') {
      sf::RectangleShape shape(sf::Vector2f(side, side));
      shape.setPosition(sf::Vector2f(side * (i % 40), side * (i / 40)));
      if (description[i] == 'X') {
        shape.setFillColor(sf::Color::Red);
      } else if (description[i] == 'O') {
        _numberOfEdibles += 1;
        shape.setFillColor(sf::Color::Green);
      }
      _shapes.push_back(shape);
    }
  }
}

Level::~Level() {
  delete [] _description;
}

bool Level::checkPosition(Worm& worm) {
  sf::Vector2u position = worm.getPosition();
  const int index = position.x + position.y * _width;
  if (_description[index] == 'X') {
    worm.kill();
  } else if (_description[index] == 'O') {
    _description[index] = ' ';
    _numberOfEdibles -= 1;
    for (size_t i = 0; i < _shapes.size(); i += 1) {
      sf::RectangleShape shape = _shapes[i];
      sf::Vector2f pos = shape.getPosition();
      if (pos.x / _side == position.x && pos.y / _side == position.y) {
        shape.setFillColor(sf::Color::Transparent);
        _shapes[i] = shape;
        break;
      }
    }
    worm.grow();
    return true;
  }
  return false;
}

void Level::render(sf::RenderWindow& window) {
  for (auto sit = _shapes.begin(); sit != _shapes.end(); ++sit) {
    window.draw(*sit);
  }
}

bool Level::isComplete() const {
  return _numberOfEdibles == 0;
}
