#ifndef LEVEL_HPP
#define LEVEL_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include "Worm.hpp"

class Level {
public:
  Level(int side, int width, int height, const char* description);
  virtual ~Level();
  bool checkPosition(Worm& worm);
  void render(sf::RenderWindow& window);
  bool isComplete() const;
  virtual Level* getNext() = 0;

private:
  std::vector<sf::RectangleShape> _shapes;
  int _numberOfEdibles;
  const int _width;
  const int _side;
  char* _description;
};

#endif // LEVEL_HPP
