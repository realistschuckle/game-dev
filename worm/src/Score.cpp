#include "Score.hpp"

short numbers[10][5][5] = {
    {{0, 1, 1, 1, 0},
     {0, 1, 0, 1, 0},
     {0, 1, 0, 1, 0},
     {0, 1, 0, 1, 0},
     {0, 1, 1, 1, 0}},
    {{0, 0, 1, 0, 0},
     {0, 1, 1, 0, 0},
     {0, 0, 1, 0, 0},
     {0, 0, 1, 0, 0},
     {0, 1, 1, 1, 0}},
    {{0, 1, 1, 1, 0},
     {0, 0, 0, 1, 0},
     {0, 1, 1, 1, 0},
     {0, 1, 0, 0, 0},
     {0, 1, 1, 1, 0}},
    {{0, 1, 1, 1, 0},
     {0, 0, 0, 1, 0},
     {0, 1, 1, 1, 0},
     {0, 0, 0, 1, 0},
     {0, 1, 1, 1, 0}},
    {{0, 1, 0, 1, 0},
     {0, 1, 0, 1, 0},
     {0, 1, 1, 1, 0},
     {0, 0, 0, 1, 0},
     {0, 0, 0, 1, 0}},
    {{0, 1, 1, 1, 0},
     {0, 1, 0, 0, 0},
     {0, 1, 1, 1, 0},
     {0, 0, 0, 1, 0},
     {0, 1, 1, 1, 0}},
    {{0, 1, 1, 1, 0},
     {0, 1, 0, 0, 0},
     {0, 1, 1, 1, 0},
     {0, 1, 0, 1, 0},
     {0, 1, 1, 1, 0}},
    {{0, 1, 1, 1, 0},
     {0, 0, 0, 1, 0},
     {0, 0, 1, 0, 0},
     {0, 0, 1, 0, 0},
     {0, 0, 1, 0, 0}},
    {{0, 1, 1, 1, 0},
     {0, 1, 0, 1, 0},
     {0, 0, 1, 0, 0},
     {0, 1, 0, 1, 0},
     {0, 1, 1, 1, 0}},
    {{0, 1, 1, 1, 0},
     {0, 1, 0, 1, 0},
     {0, 1, 1, 1, 0},
     {0, 0, 0, 1, 0},
     {0, 1, 1, 1, 0}},
};

Score::Score(float offset) {
  _score = -1;
  for (int i = 0; i < 25; i += 1) {
    _pixels[i].setSize(sf::Vector2f(20.f, 20.f));
    _pixels[i].setPosition(
        sf::Vector2f(offset + (i % 5) * 20.f, 20.f + (i / 5) * 20.f));
  }
  increment();
}

void Score::increment() {
  _score += 1;
  for (int i = 0; i < 25; i += 1) {
    if (numbers[_score][i / 5][i % 5] == 0) {
      _pixels[i].setFillColor(sf::Color::Black);
    } else if (_score < 9) {
      _pixels[i].setFillColor(sf::Color::White);
    } else {
      _pixels[i].setFillColor(sf::Color::Green);
    }
  }
}

void Score::decrement() {
  _score -= 1;
  for (int i = 0; i < 25; i += 1) {
    if (numbers[_score][i / 5][i % 5] == 0) {
      _pixels[i].setFillColor(sf::Color::Black);
    } else if (_score < 9) {
      _pixels[i].setFillColor(sf::Color::White);
    } else {
      _pixels[i].setFillColor(sf::Color::Green);
    }
  }
}

void Score::draw(sf::RenderWindow& window) {
  for (int i = 0; i < 25; i += 1) {
    window.draw(_pixels[i]);
  }
}

int Score::value() const {
  return _score;
}
