#ifndef WORM_RENDERER_HPP
#define WORM_RENDERER_HPP

#include <SFML/Graphics.hpp>
#include "Worm.hpp"

class WormRenderer {
 public:
  WormRenderer(Worm& worm, sf::RenderWindow& window, const sf::Color& color,
               int side);
  void render() const;

 private:
  const Worm& _worm;
  sf::RenderWindow& _window;
  const int _side;
  const sf::Color _color;
};

#endif  // WORM_RENDERER_HPP
