#ifndef WORM_HPP
#define WORM_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>

enum WormTurn {
  LEFT = -2,
  UP = -1,
  DOWN = 1,
  RIGHT = 2,
};

class Worm {
 public:
  Worm(sf::Vector2u position, WormTurn direction);
  void grow();
  void turn(WormTurn direction);
  void update();
  sf::Vector2u getPosition() const;
  std::vector<int>::const_reverse_iterator segsbegin() const;
  std::vector<int>::const_reverse_iterator segsend() const;
  WormTurn getDirection() const;
  bool isDead() const;
  void kill();
  void reset(sf::Vector2u position, WormTurn direction);

  friend std::ostream& operator<<(std::ostream& out, const Worm& worm);

 private:
  int _capacity;
  std::vector<int> _segments;
  sf::Vector2u _position;
  WormTurn _direction;
  bool _isDead;
};

#endif  // WORM_HPP
