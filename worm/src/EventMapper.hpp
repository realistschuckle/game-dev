#if !defined(EVENT_MAPPER_HPP)
#define EVENT_MAPPER_HPP

#include <SFML/Graphics.hpp>
#include <map>

class Command {
 public:
  virtual ~Command();
  virtual void invoke() = 0;
};

class EventMapper {
 public:
  EventMapper();
  ~EventMapper();
  void add(sf::Keyboard::Key input, Command* handler);
  void commit();
  void handle(sf::Keyboard::Key input);

 private:
  std::map<sf::Keyboard::Key, Command*> _handlers;
  sf::Keyboard::Key _currentKey;
};

#endif  // EVENT_MAPPER_HPP
