#ifndef SCORE_HPP
#define SCORE_HPP

#include <SFML/Graphics.hpp>

class Score {
 public:
  Score(float offset);
  void increment();
  void decrement();
  void draw(sf::RenderWindow& window);
  int value() const;

 private:
  int _score;
  sf::RectangleShape _pixels[25];
};

#endif  // SCORE_HPP
