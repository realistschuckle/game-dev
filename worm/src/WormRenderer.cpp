#include "WormRenderer.hpp"

WormRenderer::WormRenderer(Worm& worm, sf::RenderWindow& window,
                           const sf::Color& color, int side)
    : _worm(worm), _window(window), _side(side), _color(color) {}

void WormRenderer::render() const {
  WormTurn lastDirection = _worm.getDirection();
  WormTurn direction;
  sf::Vector2u pos = _worm.getPosition();

  for (auto hit = _worm.segsbegin(); hit != _worm.segsend(); hit += 1) {
    sf::RectangleShape segment;
    segment.setFillColor(_color);
    if (_worm.isDead()) {
      segment.setFillColor(sf::Color::Red);
    }
    if (hit == _worm.segsbegin()) {
      direction = _worm.getDirection();
      switch (direction) {
        case WormTurn::UP: {
          segment.setPosition(pos.x * _side, pos.y * _side);
          pos.y += abs(*hit);
          break;
        }
        case WormTurn::LEFT: {
          segment.setPosition(pos.x * _side, pos.y * _side);
          pos.x += abs(*hit);
          break;
        }
        case WormTurn::DOWN: {
          segment.setPosition(pos.x * _side, (pos.y - (*hit) + 1) * _side);
          pos.y -= abs(*hit);
          break;
        }
        case WormTurn::RIGHT: {
          segment.setPosition((pos.x - (*hit) + 1) * _side, pos.y * _side);
          pos.x -= abs(*hit);
          break;
        }
      }
      switch (direction) {
        case WormTurn::UP:
        case WormTurn::DOWN: {
          segment.setSize(sf::Vector2f(_side, abs(*hit) * _side));
          break;
        }
        case WormTurn::LEFT:
        case WormTurn::RIGHT: {
          segment.setSize(sf::Vector2f(abs(*hit) * _side, _side));
          break;
        }
      }
    } else {
      switch (lastDirection) {
        case WormTurn::UP:
        case WormTurn::DOWN: {
          if ((*hit) > 0) {
            direction = WormTurn::RIGHT;
          } else {
            direction = WormTurn::LEFT;
          }
          break;
        }
        case WormTurn::LEFT:
        case WormTurn::RIGHT: {
          if ((*hit) > 0) {
            direction = WormTurn::DOWN;
          } else {
            direction = WormTurn::UP;
          }
          break;
        }
      }
      switch (direction) {
        case WormTurn::UP: {
          segment.setPosition(pos.x * _side, pos.y * _side);
          pos.y += abs(*hit);
          break;
        }
        case WormTurn::LEFT: {
          segment.setPosition(pos.x * _side, pos.y * _side);
          pos.x += abs(*hit);
          break;
        }
        case WormTurn::DOWN: {
          pos.y -= (abs(*hit) - 1);
          segment.setPosition(pos.x * _side, pos.y * _side);
          pos.y -= 1;
          break;
        }
        case WormTurn::RIGHT: {
          pos.x -= (abs(*hit) - 1);
          segment.setPosition(pos.x * _side, pos.y * _side);
          pos.x -= 1;
          break;
        }
      }
      switch (direction) {
        case WormTurn::UP:
        case WormTurn::DOWN: {
          segment.setSize(sf::Vector2f(_side, abs(*hit) * _side));
          break;
        }
        case WormTurn::LEFT:
        case WormTurn::RIGHT: {
          segment.setSize(sf::Vector2f(abs(*hit) * _side, _side));
          break;
        }
      }
    }
    lastDirection = direction;
    _window.draw(segment);
  }
}
