#include "KeyboardWormCommand.hpp"

KeyboardWormCommand::KeyboardWormCommand(Worm& worm, WormTurn turn)
    : _worm(worm), _turn(turn) {}

KeyboardWormCommand::~KeyboardWormCommand() {}

void KeyboardWormCommand::invoke() { _worm.turn(_turn); }
