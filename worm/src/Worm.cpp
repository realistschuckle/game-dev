#include "Worm.hpp"

Worm::Worm(sf::Vector2u position, WormTurn direction)
    : _capacity(8), _position(position), _direction(direction), _isDead(false) {
  reset(position, direction);
}

void Worm::grow() { _capacity += 2; }

void Worm::turn(WormTurn direction) {
  if (direction != -_direction) {
    if (_direction != direction && _segments.back() != 0) {
      _segments.push_back(0);
    }
    _direction = direction;
  }
}

void Worm::update() {
  *(_segments.end() - 1) += 1 * abs(_direction) / _direction;
  int sum = 0;
  for (auto rit = _segments.crbegin(); rit != _segments.crend(); ++rit) {
    sum += abs(*rit);
  }
  switch (_direction) {
    case WormTurn::UP:
      _position.y -= 1;
      break;
    case WormTurn::DOWN:
      _position.y += 1;
      break;
    case WormTurn::LEFT:
      _position.x -= 1;
      break;
    case WormTurn::RIGHT:
      _position.x += 1;
      break;
  }
  if (sum > _capacity) {
    _segments.front() -= abs(_segments.front()) / _segments.front();
  }
  if (_segments.front() == 0) {
    _segments.erase(_segments.begin());
  }

  bool vertical = false;
  sf::Vector2u pieces = _position;
  _isDead = false;
  for (auto rit = _segments.crbegin(); rit != _segments.crend(); ++rit) {
    if (rit == _segments.crbegin()) {
      switch (_direction) {
        case WormTurn::UP:
          pieces.y -= *rit;
          vertical = true;
          break;
        case WormTurn::DOWN:
          pieces.y -= *rit;
          vertical = true;
          break;
        case WormTurn::LEFT:
          pieces.x -= *rit;
          break;
        case WormTurn::RIGHT:
          pieces.x -= *rit;
          break;
      }
      continue;
    }

    vertical = !vertical;
    const int bound = rit + 1 == _segments.crend() ? abs(*rit) - 1 : abs(*rit);
    for (int i = 0; i < bound; i += 1) {
      if (vertical) {
        pieces.y -= abs(*rit) / (*rit);
      } else {
        pieces.x -= abs(*rit) / (*rit);
      }
      if (pieces == _position) {
        _isDead = true;
        break;
      }
    }
  }
}

sf::Vector2u Worm::getPosition() const { return _position; }

WormTurn Worm::getDirection() const { return _direction; }

bool Worm::isDead() const { return _isDead; }

void Worm::kill() {
  _isDead = true;
}

void Worm::reset(sf::Vector2u position, WormTurn direction) {
  _isDead = false;
  _position = position;
  _direction = direction;
  _segments.clear();
  _segments.push_back(0);
  _capacity = 8;
  switch (_direction) {
    case WormTurn::UP:
      _position.y += 1;
      break;
    case WormTurn::DOWN:
      _position.y -= 1;
      break;
    case WormTurn::LEFT:
      _position.x += 1;
      break;
    case WormTurn::RIGHT:
      _position.x -= 1;
      break;
  }
}

std::vector<int>::const_reverse_iterator Worm::segsbegin() const {
  return _segments.crbegin();
}

std::vector<int>::const_reverse_iterator Worm::segsend() const {
  return _segments.crend();
}

std::ostream& operator<<(std::ostream& out, const Worm& worm) {
  out << "#Worm{";
  switch (worm._direction) {
    case WormTurn::UP:
      out << "U:";
      break;
    case WormTurn::DOWN:
      out << "D:";
      break;
    case WormTurn::LEFT:
      out << "L:";
      break;
    case WormTurn::RIGHT:
      out << "R:";
      break;
  }
  out << worm._position.x << "," << worm._position.y << ":";
  for (auto rit = worm._segments.crbegin(); rit != worm._segments.crend();
       ++rit) {
    out << *rit << ",";
  }
  out << "}";
  return out;
}
