#if !defined(KEYBOARD_WORM_COMMAND_HPP)
#define KEYBOARD_WORM_COMMAND_HPP

#include "Worm.hpp"
#include "EventMapper.hpp"

class KeyboardWormCommand : public Command {
 public:
  KeyboardWormCommand(Worm& worm, WormTurn turn);
  virtual ~KeyboardWormCommand();
  virtual void invoke();

 private:
  Worm& _worm;
  WormTurn _turn;
};

#endif // KEYBOARD_WORM_COMMAND_HPP
