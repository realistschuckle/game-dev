#include <SFML/Graphics.hpp>
#include <iostream>
#include "EventMapper.hpp"
#include "KeyboardWormCommand.hpp"
#include "Worm.hpp"
#include "WormRenderer.hpp"
#include "levels/Level01.hpp"

int main() {
  const int side = 20;
  const int width = 40;
  const int height = 30;
  bool gamePaused = false;
  bool gameOver = false;
  float speed = 0.5;

  sf::RenderWindow window(sf::VideoMode(width * side, height * side), "WORMS");
  window.setVerticalSyncEnabled(true);

  sf::Clock clock;
  Worm human(sf::Vector2u(5, 15), WormTurn::RIGHT);
  WormRenderer humanRenderer(human, window, sf::Color::White, side);
  EventMapper mapper;
  Level01 level(side, width, height);
  Level* currentLevel = &level;

  mapper.add(sf::Keyboard::Up, new KeyboardWormCommand(human, WormTurn::UP));
  mapper.add(sf::Keyboard::Down,
             new KeyboardWormCommand(human, WormTurn::DOWN));
  mapper.add(sf::Keyboard::Left,
             new KeyboardWormCommand(human, WormTurn::LEFT));
  mapper.add(sf::Keyboard::Right,
             new KeyboardWormCommand(human, WormTurn::RIGHT));

  while (window.isOpen()) {
    sf::Event event;

    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) {
        window.close();
      } else if (event.type == sf::Event::KeyPressed) {
        if (event.key.code == sf::Keyboard::Q && event.key.control) {
          window.close();
          continue;
        }
        if (event.key.code == sf::Keyboard::Escape) {
          gamePaused = !gamePaused;
        }
        mapper.handle(event.key.code);
      }
    }

    window.clear(sf::Color(32, 32, 32, 255));

    sf::Time time = clock.getElapsedTime();

    if (time.asSeconds() > speed && !gamePaused && !gameOver) {
      mapper.commit();
      human.update();
      if (currentLevel->checkPosition(human)) {
        speed -= .02;
      }
      if (currentLevel->isComplete()) {
        currentLevel = currentLevel->getNext();
        human.reset(sf::Vector2u(5, 15), WormTurn::RIGHT);
        speed = 0.5;
      }
      gameOver = currentLevel == nullptr || human.isDead() ||
                 currentLevel->isComplete();
      clock.restart();
    }

    humanRenderer.render();
    if (currentLevel != nullptr) {
      currentLevel->render(window);
    }

    window.display();
  }

  return 0;
}
