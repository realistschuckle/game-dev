# Curtis' Game Dev Journey: WORM

I used to play worm on my Timex-Sinclair 1000. It was fun.

## Game Play

* Ctrl+Q: Quit
* Esc: Pause
* Up: Move worm up
* Down: Move worm down
* Left: Move worm left
* Right: Move worm right

Stay away from red bricks. They will kill you and end the game.

Eat all of the green fruit to advance to the next level.
