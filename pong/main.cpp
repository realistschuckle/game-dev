#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>

short numbers[10][5][5] = {
  {
    {0, 1, 1, 1, 0},
    {0, 1, 0, 1, 0},
    {0, 1, 0, 1, 0},
    {0, 1, 0, 1, 0},
    {0, 1, 1, 1, 0}
  },
  {
    {0, 0, 1, 0, 0},
    {0, 1, 1, 0, 0},
    {0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0},
    {0, 1, 1, 1, 0}
  },
  {
    {0, 1, 1, 1, 0},
    {0, 0, 0, 1, 0},
    {0, 1, 1, 1, 0},
    {0, 1, 0, 0, 0},
    {0, 1, 1, 1, 0}
  },
  {
    {0, 1, 1, 1, 0},
    {0, 0, 0, 1, 0},
    {0, 1, 1, 1, 0},
    {0, 0, 0, 1, 0},
    {0, 1, 1, 1, 0}
  },
  {
    {0, 1, 0, 1, 0},
    {0, 1, 0, 1, 0},
    {0, 1, 1, 1, 0},
    {0, 0, 0, 1, 0},
    {0, 0, 0, 1, 0}
  },
  {
    {0, 1, 1, 1, 0},
    {0, 1, 0, 0, 0},
    {0, 1, 1, 1, 0},
    {0, 0, 0, 1, 0},
    {0, 1, 1, 1, 0}
  },
  {
    {0, 1, 1, 1, 0},
    {0, 1, 0, 0, 0},
    {0, 1, 1, 1, 0},
    {0, 1, 0, 1, 0},
    {0, 1, 1, 1, 0}
  },
  {
    {0, 1, 1, 1, 0},
    {0, 0, 0, 1, 0},
    {0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0}
  },
  {
    {0, 1, 1, 1, 0},
    {0, 1, 0, 1, 0},
    {0, 0, 1, 0, 0},
    {0, 1, 0, 1, 0},
    {0, 1, 1, 1, 0}
  },
  {
    {0, 1, 1, 1, 0},
    {0, 1, 0, 1, 0},
    {0, 1, 1, 1, 0},
    {0, 0, 0, 1, 0},
    {0, 1, 1, 1, 0}
  },
};

class Score
{
public:
  Score(float offset)
  {
    score = -1;
    for (int i = 0; i < 25; i += 1)
    {
      pixels[i].setSize(sf::Vector2f(20.f, 20.f));
      pixels[i].setPosition(sf::Vector2f(offset + (i % 5) * 20.f, 20.f + (i / 5) * 20.f));
    }
    increment();
  }
  void increment()
  {
    score += 1;
    for (int i = 0; i < 25; i += 1)
    {
      if (numbers[score][i / 5][i % 5] == 0)
      {
        pixels[i].setFillColor(sf::Color::Black);
      }
      else if (score < 9)
      {
        pixels[i].setFillColor(sf::Color::White);
      }
      else
      {
        pixels[i].setFillColor(sf::Color::Green);
      }
    }
  }
  void draw(sf::RenderWindow& window)
  {
    for (int i = 0; i < 25; i += 1)
    {
      window.draw(pixels[i]);
    }
  }
  int value() const { return score; }

private:
  int score;
  sf::RectangleShape pixels[25];
};

class Ball
{
public:
  Ball()
  {
    thing.setSize(sf::Vector2f(20.f, 20.f));
    reset();
  }
  void draw(sf::RenderWindow& window) { window.draw(thing); }
  void impact(bool x, bool y, float delta) {
    if (x) { velocity.x = -velocity.x; }
    if (y) { velocity.y = -velocity.y; }
    velocity.y += delta;
    velocity.x += 0.1f * delta;
    thing.move(velocity);
  }
  float left() const { return thing.getPosition().x; }
  float right() const { return left() + 20.f; }
  float bottom() const { return thing.getPosition().y; }
  float top() const { return bottom() + 20.f; }
  void reset()
  {
    thing.setPosition(390.f, 290.f);
    velocity.x = (rand() % 3 + 2) * ((rand() % 2) == 0 ? -1 : 1);
    velocity.y = (rand() % 3 + 2) * ((rand() % 2) == 0 ? -1 : 1);
  }
  void update()
  {
    if (bottom() < 0.f) impact(false, true, 0);
    if (top() > 600.f) impact(false, true, 0);
    thing.move(velocity);
  }

private:
  sf::RectangleShape thing;
  sf::Vector2f velocity;
};

class Paddle
{
public:
  Paddle(float paddleOffset, float scoreOffset) : score(scoreOffset)
  {
    moveDelta = 7.5f;
    movingUp = false;
    movingDown = false;
    thing.setSize(sf::Vector2f(20.f, 80.f));
    thing.setPosition(sf::Vector2f(paddleOffset, 260.f));
  }
  void draw(sf::RenderWindow& window)
  {
     window.draw(thing);
     score.draw(window);
  }
  void moveUp(bool value) { movingUp = value; }
  void moveDown(bool value) { movingDown = value; }
  float left() const { return thing.getPosition().x; }
  float right() const { return left() + 20.f; }
  float bottom() const { return thing.getPosition().y; }
  float top() const { return bottom() + 80.f; }
  float unitVelocity() { return (movingUp ? -1 : 0) + (movingDown ? 1 : 0); }
  virtual void update()
  {
    if (movingUp) thing.move(0, -moveDelta);
    if (movingDown) thing.move(0, moveDelta);
    if (top() > 600.f) thing.setPosition(left(), 520.f);
    if (bottom() < 0.f) thing.setPosition(left(), 0.f);
  }
  virtual bool testCollision(Ball& ball)
  {
    if (ball.left() < right() && ball.right() > left() && ball.bottom() < top() && ball.top() > bottom())
    {
      ball.impact(true, false, unitVelocity());
      return true;
    }
    return false;
  }
  void goal()
  {
    score.increment();
  }
  int getScore() const { return score.value(); }

protected:
  bool movingUp;
  bool movingDown;
  float moveDelta;

private:
  sf::RectangleShape thing;
  Score score;
};

class AiPaddle : public Paddle
{
public:
  AiPaddle(float o, float x) : Paddle(o, x)
  {
    moveDelta = 5.f;
  }
  virtual bool testCollision(Ball& ball)
  {
    ballTop = ball.top();
    ballBottom = ball.bottom();
    return Paddle::testCollision(ball);
  }
  virtual void update()
  {
    movingUp = false;
    movingDown = false;
    if (ballTop > top())
    {
      movingDown = true;
      float d = ballTop - top();
      if (d < 0) d = -d;
      moveDelta = std::min(5.0f, d);
    }
    else if (ballBottom < bottom())
    {
      movingUp = true;
      float d = ballBottom - bottom();
      if (d < 0) d = -d;
      moveDelta = std::min(5.0f, d);
    }
    Paddle::update();
  }

private:
  float ballTop;
  float ballBottom;
};

int main() {
  bool gameOver = false;
  bool paused = false;

  sf::RenderWindow window(sf::VideoMode(800, 600), "PONG");
  window.setVerticalSyncEnabled(true);

  sf::SoundBuffer buffer;
  if (!buffer.loadFromFile("blip.ogg"))
  {
    return -1;
  }

  sf::Sound blip;
  blip.setBuffer(buffer);

  Paddle left(20.f, 280.f);
  AiPaddle right(760.f, 420.f);
  Ball ball;

  sf::RectangleShape court(sf::Vector2f(800.f, 600.f));
  court.setPosition(0.f, 0.f);
  court.setFillColor(sf::Color::Black);

  sf::RectangleShape net(sf::Vector2f(20.f, 600.f));
  net.setPosition(390.f, 0.f);
  net.setFillColor(sf::Color::Red);

  while (window.isOpen())
  {
    sf::Event event;
    while (window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
      {
        window.close();
      }
      else if (!gameOver && event.type == sf::Event::KeyPressed)
      {
        if (event.key.code == sf::Keyboard::Up) left.moveUp(true);
        if (event.key.code == sf::Keyboard::Down) left.moveDown(true);
        if (event.key.code == sf::Keyboard::Escape) paused = !paused;
        if (event.key.code == sf::Keyboard::Q && event.key.control) window.close();
      }
      else if (!gameOver && event.type == sf::Event::KeyReleased)
      {
        if (event.key.code == sf::Keyboard::Up) left.moveUp(false);
        if (event.key.code == sf::Keyboard::Down) left.moveDown(false);
      }
    }

    window.clear(sf::Color(32, 32, 32, 255));
    window.draw(court);

    if (!gameOver && !paused)
    {
      left.update();
      right.update();
      ball.update();

      if (left.testCollision(ball))
      {
        blip.play();
      }
      if (right.testCollision(ball))
      {
        blip.play();
      }

      if (ball.left() > 800.f)
      {
        left.goal();
        ball.reset();
      }
      if (ball.right() < 0.f)
      {
        right.goal();
        ball.reset();
      }
      if (left.getScore() == 9 || right.getScore() == 9)
      {
        gameOver = true;
      }
    }

    window.draw(net);
    left.draw(window);
    right.draw(window);
    ball.draw(window);

    window.display();
  }

  return 0;
}
